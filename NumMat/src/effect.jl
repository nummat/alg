"""
The context for the effect
"""
mutable struct EffectContext
  name
  handler
end

"""
  perform_effect, add_handler = create_effect(name)

Create a new effect with the given name.  
"""
function create_effect(name)
  context = EffectContext(name, nothing)
  function perform_effect(params...)
    if isnothing(context)
      throw("Missing effect handler for $(contex.name)")
    end
    context.handler(params...)
  end
  function add_handler(handler)
    context.handler = handler
  end
  return perform_effect, add_handler
end