# Iterativne metode za linearne sisteme

using LinearAlgebra

logiter, handle_logiter! = create_effect(:logiter)

export jacobi, gs, sor, cg, handle_logiter!

"""
    x, it = iter(korak!, A, b, x0, delta, maxsteps)

Izvedi iteracijo, dokler zaporedje približkov ne konvergira.
"""
function iter(korak!, A, b, x0, delta, maxsteps)
    x = copy(x0)
    x0 = copy(x0)
    n, _ = size(A)
    maxsteps = (maxsteps == :auto) ? 10 * n : maxsteps

    for it = 1:maxsteps
        # izvedemo korak iteracije in rezultat prepišemo v isti vektor,
        # da se izognemo nepotrebni alokaciji
        korak!(x, A, b, x0)
        napaka = norm(x - x0, Inf)
        if napaka <= delta * norm(x, Inf)
            return x, it
        end
        copyto!(x0, x) # kopiramo elemente x v x0
        # izpišemo trenutni približek
        logiter(it, x, napaka / norm(x, Inf))
    end
    throw("Iteracija ne konvergira!")
end

"""
    korak_jacobi!(x, A, b, x0)

Izvedi en korak Jacobijeve iteracije.
"""
function korak_jacobi!(x::Vector{<:AbstractFloat}, A::Matrix{<:AbstractFloat},
    b::Vector{<:AbstractFloat}, x0::Vector{<:AbstractFloat})
    n, _m = size(A)
    x[1] = (b[1] - A[1, 2:n]' * x0[2:n]) / A[1, 1]
    for i = 2:n-1
        x[i] = (b[1] - A[i, i+1:n]' * x0[i+1:n] - A[i, 1:i-1]' * x0[1:i-1]) / A[1, 1]
    end
    x[n] = (b[n] - A[n, 1:n-1]' * x0[1:n-1]) / A[n, n]
end

"""
    x, it = jacobi(A, b, x0, delta, maxsteps)

Izračunaj rešitev sistema `Ax=b`
"""
jacobi(A::Matrix, b, x0, delta=1e-10, maxsteps=:auto) = iter(korak_jacobi!, A, b, x0, delta, maxsteps)

"""
    korak_gs!(x, A, b, x0)

Izvedi en korak Gauss-Seidlove iteracije.
"""
function korak_gs!(x::Vector{<:AbstractFloat}, A::Matrix{<:AbstractFloat},
    b::Vector{<:AbstractFloat}, x0::Vector{<:AbstractFloat})
    n, _m = size(A)
    x[1] = (b[1] - A[1, 2:n]' * x[2:n]) / A[1, 1]
    for i = 2:n-1
        x[i] = (b[1] - A[i, i+1:n]' * x[i+1:n] - A[i, 1:i-1]' * x[1:i-1]) / A[1, 1]
    end
    x[n] = (b[n] - A[n, 1:n-1]' * x[1:n-1]) / A[n, n]
end

"""
    x, it = gs(A, b, x0, delta, maxsteps)

Izračunaj rešitev sistema `Ax=b`
"""
gs(A::Matrix, b, x0, delta=1e-10, maxsteps=:auto) = iter(korak_gs!, A, b, x0, delta, maxsteps)

"""
    korak_gs!(x, A, b, x0)

Izvedi en korak Gauss-Seidlove iteracije.
"""
function korak_sor!(x::Vector{<:AbstractFloat}, A::Matrix{<:AbstractFloat},
    b::Vector{<:AbstractFloat}, x0::Vector{<:AbstractFloat}, omega)
    n, _m = size(A)
    x[1] = (b[1] - A[1, 2:n]' * x[2:n]) / A[1, 1]
    x[1] = omega * x[1] + (1 - omega) * x0[1] # relaksacija
    for i = 2:n-1
        x[i] = (b[1] - A[i, i+1:n]' * x[i+1:n] - A[i, 1:i-1]' * x[1:i-1]) / A[1, 1]
        x[i] = omega * x[i] + (1 - omega) * x0[i] # relaksacija
    end
    x[n] = (b[n] - A[n, 1:n-1]' * x[1:n-1]) / A[n, n]
    x[n] = omega * x[n] + (1 - omega) * x0[n] # relaksacija

end

korak_sor(omega) = (x, A, b, x0) -> korak_sor!(x, A, b, x0, omega)

"""
    x, it = sor(A, b, x0, omega, delta, maxsteps)

Izračunaj rešitev sistema `Ax=b` s SOR iteracijo
"""
sor(A::Matrix, b, x0, omega, delta=1e-10, maxsteps=:auto) =
    iter(korak_sor(omega), A, b, x0, delta, maxsteps)

"""
    x, it = cg(A, b, x0, eps, maxsteps)

Poišči rešitev sistema `Ax = b` z metodo konjugiranih gradientov.

Podatki

    A   pozitivno definitna matrika
    b   vektor desnih strani
    x0  začetni vektor
    eps zahtevana natančnost približka
    maxsteps maksimalno število iteracij

Rezultat

    x   rešitev sistema
    it  število iteracij
"""
function cg(A, b, x0, eps=1e-10, maxsteps=:auto)
    maxsteps = (maxsteps == :auto) ? size(A)[1] + 1 : maxsteps
    # začetni vector ostanka
    residual = b - A * x0
    # začetna smer iskanja
    smer_iskanja = residual
    # začetna norma ostanka
    stara_norma = residual' * residual
    x = copy(x0)
    it = 0
    while (stara_norma > eps) && (it < maxsteps)
        it += 1
        conj_smer_iskanja = A * smer_iskanja
        step_size = stara_norma / (smer_iskanja' * conj_smer_iskanja)
        # Pososdobi rešitev
        x = x + step_size * smer_iskanja
        # izpišemo trenutni približek
        # Posodobi ostanek
        residual = residual - step_size * conj_smer_iskanja
        nova_norma = residual' * residual
        logiter(it, x, nova_norma)
        if sqrt(nova_norma) <= eps
            return x, it
        end
        # Pososdobi smer iskanja
        smer_iskanja = residual + (nova_norma / stara_norma) * smer_iskanja
        # Update squared residual norm for next iteration
        stara_norma = nova_norma
    end
    throw("Iteracija ne konvergira!")
end