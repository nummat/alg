#' # Iterativne metode
#' Predstavitem delovanja Jacobijeve iteracije
using NumMat, LinearAlgebra

println("\nOptimalni omega za SOR:\n")

println("""
[3   1 0 ...  0 ]
[1  3  1 ...  0 ]
[ ...           ]
[0  ....    1 3 ]
""")
sleep(1)

n = 20
A = diagm(-1 => ones(n - 1), 0 => 3 * ones(n), 1 => ones(n - 1))
D = diagm(diag(A))
L = tril(A, -1)
U = triu(A, 1)
b = 1.0 * ones(n)

x0 = b

handle_logiter!((it, x, err) -> nothing)

omega = 1.05
x, it = sor(A, b, x0, omega, 1e-8, 10000)
println("SOR iteracija (ω = $omega): $it")
println("Spektralna norma matrike R je $(opnorm((omega*L + D)\((1-omega)*D - omega*U)))")

function iter_sor(A, b)
  omegas = range(0.1, 1.9, 200)
  iter = []
  for omega in omegas
    x, it = sor(A, b, b, omega, 1e-5, 10000)
    push!(iter, it)
  end
  return omegas, iter
end

using Plots
omegas, iter = iter_sor(A, b)
plot(omegas, iter)
