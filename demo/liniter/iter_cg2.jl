using Plots, NumMat

println("""
Primer reševanja sistema z metodo konjugiranih gradientov.
""")
sleep(1)

A = [2 1; 1 5]
x = [1.0, 2.0]
b = A * x

x0 = [3.5, 2]
priblizki = [x0]
function logiter(it, x, err)
  push!(priblizki, x)
  println("$it: $x (err: $err)")
end
handle_logiter!(logiter)

xp, it = cg(A, b, x0)

plt = contour(range(-2, 4, 100), range(0, 4, 100),
  (x, y) -> [x, y]' * A * [x, y] / 2 - b' * [x, y],
  title="Metoda konjugiranih gradientov",
  aspect_ratio=:equal)
scatter!([x[1]], [x[2]], label=false)
t, s = [x[1] for x in priblizki], [x[2] for x in priblizki]
plot!(t, s, label="konj. gradient")

function grad_spust(A, b, x0)
  priblizki = [x0]
  for i = 1:5
    grad = b - A * x0
    korak = (grad' * grad) / (grad' * (A * grad))
    x0 += korak * grad
    push!(priblizki, x0)
  end
  return priblizki
end

prib_grad = grad_spust(A, b, x0)
tg, sg = [x[1] for x in prib_grad], [x[2] for x in prib_grad]
plot!(tg, sg, label="gradient")