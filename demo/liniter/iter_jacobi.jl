#' # Iterativne metode
#' Predstavitem delovanja Jacobijeve iteracije
using NumMat, LinearAlgebra

println("\nJacobijeva iteracija za testni primer:\n")

println("""
12x-3y+z=10,
-x+9y+2z=10,
x-y+10z=10.
""")
sleep(1)

A = [12 -3 1; -1 9 2; 1 -1 10.0]
D = diagm(diag(A))
L = -tril(A, -1)
U = -triu(A, 1);
b = [10.0; 10; 10];

x0 = [0.5, 0.5, 0.5]
println("Zacetni priblizek: $x0")
sleep(1)

handle_logiter!((it, x, err) -> println("$it: $x ($err)"))

x, it = jacobi(A, b, x0)
println("Supremum norma matrike R je $(norm(inv(D)*(L+U),Inf))")