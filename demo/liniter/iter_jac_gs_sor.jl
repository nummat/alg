#' # Iterativne metode
#' Predstavitem delovanja Jacobijeve iteracije
using NumMat, LinearAlgebra

println("\nPrimerjava iteracijskih metod:\n")

println("""
[1   1/n 1/n ...  1/n ]
[1/n  1  1/n ...  1/n ]
[ ...                 ]
[1/n  1/n  .... 1/n 1 ]
""")
sleep(1)

n = 20
A = diagm((1 - 1 / n) * ones(n)) + 1 / n * ones(n, n)
D = diagm(diag(A))
L = tril(A, -1)
U = triu(A, 1)
b = 1.0 * ones(n)

x0 = b

handle_logiter!((it, x, err) -> nothing)

x, it = jacobi(A, b, x0, 1e-8, 1000)
println("Jacobijeva iteracija: $it")
println("Spektralna norma matrike R je $(opnorm((D)\(L+U)))")
x, it = gs(A, b, x0, 1e-8, 1000)
println("Gauss-Seidlova iteracija: $it")
println("Spektralna norma matrike R je $(opnorm((D+L)\U))")
omega = 0.9
x, it = sor(A, b, x0, omega, 1e-8, 1000)
println("SOR iteracija (ω = $omega): $it")
println("Spektralna norma matrike R je $(opnorm((omega*L + D)\((1-omega)*D - omega*U)))")