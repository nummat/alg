#' # Iterativne metode
#' Predstavitem delovanja metode konjugirnih gradientov
using NumMat

println("\nMetoda konjugiranih gradientov za testni primer:\n")

println("""
2x -  y          = 1,
-x + 2y -z       = 1,
     -y + 2z -w  = 1,
          -z +2w = 1.
""")
sleep(1)

A = [2 -1 0 0; -1 2 -1 0; 0 -1 2 -1; 0 0 -1 2]
b = [1.0, 1, 1, 1];

x0 = [0.5, 0.5, 0.5, 0.5]
println("Zacetni priblizek: $x0")
sleep(1)

handle_logiter!((it, x, err) -> println("$it: $x (err: $err)"))

x, it = cg(A, b, x0)