% Regula falsi - Uporaba algoritma 3.3.1
% Resevanje enacbe f331(x) = 0

a = 0;
b = 2;
epsx = 1e-7;
epsy = 1e-7;
N = 30;
C = [];
clc

alg331
n = length(C);

semilogy(1:n,abs(C-C(n))), grid
  xlabel('n')
  ylabel('abs(napaka)')
figure
  plot(C), grid
  xlabel('n')
  ylabel('abs(napaka)')

C
