% Tangentna metoda - Uporaba algoritma 3.4.1
% Resevanje enacbe f341(x) = 0

a = 0;
epsy = 1e-14;
N = 30; K = 5;
C = []; F = []; clc

alg342
n = length(C);

semilogy(1:n,abs(C-C(n))), grid
  xlabel('n')
  ylabel('abs(napaka)')
figure
  plot(C), grid
  xlabel('n')
  ylabel('abs(napaka)')

C
