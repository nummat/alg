% Sekantna metoda - Uporaba algoritma 3.3.2
% Resevanje enacbe f332(x) = 0

a = 0;
b = 2;

epsy = 1e-7;
N = 30;
C = [];
clc

alg332
n = length(C);

semilogy(1:n,abs(C-C(n))), grid
  xlabel('n')
  ylabel('abs(napaka)')
figure
  plot(C), grid
  xlabel('n')
  ylabel('abs(napaka)')

C
