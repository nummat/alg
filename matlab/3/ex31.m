% Plotting a 6-th degree polynomial with more than 6 zeros

c = 0.003;
x = linspace(1-c,1+c,1000);
y = (((((x-6).*x+15).*x-20).*x+15).*x-6).*x+1;
plot(x,y), grid

