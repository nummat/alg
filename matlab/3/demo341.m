% Tangentna metoda - Uporaba algoritma 3.4.1
% Resevanje enacbe f341(x) = 0

a = 1
epsy = 1e-14;
N = 30;
C = [];
clc

alg341
n = length(C);

semilogy(1:n,abs(C-C(n))), grid
  xlabel('n')
  ylabel('abs(napaka)')
figure
  plot(C), grid
  xlabel('n')
  ylabel('abs(napaka)')

C
