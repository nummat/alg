% Lagrangeov interpolacijski polinom - uporaba algoritma 4.2.1
% Vrednost Lagrangeovega interpolacijskega polinoma

x = [1 3 6 10];
f = [3 2 6 1];
t = 9;

alg421

plot(x,f,'r*',t,p,'gx')
     title('Polinomska interpolacija - Lagrangeova formula')
     xlabel('x'), ylabel('y')
