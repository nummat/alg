% Lagrangeov interpolacijski polinom z baricentricno formulo - Berrut & Trefethen
% Vrednost Lagrangeovega interpolacijskega polinoma
% Podatki:
% 		x		interpolacijske tocke
%		f 		interpolacijske vrednosti
%		t		tocka, v kateri racunamo vrednost
% Rezultat:
%		p		vrednost interpolacijskega polinoma v tocki t

[ans,n] = size(x);	        % stevilo interpolacijskih tock
[ans,m] = size(f);		% stevilo interpolacijskih vrednosti
if n==m
n = n-1;
W = zeros(n+1,n+1); 
W(1,1) = 1;
    for j=2:n+1
        for k=1:j
        W(j,k) = (x(k)-x(j))*W(j-1,k);
        end
    W(j,j) = prod(x(j)-x(1:j-1));
end
    for j=1:n+1
    W(n+1,j)=1/W(n+1,j);
    end

p = (W(n+1,:)./(t-x)*f')/sum(W(n+1,:)./(t-x));
		else
	  error('Napacno stevilo podatkov')
end
