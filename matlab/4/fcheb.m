% Izračun vrednosti funkcije v točki x
%
% Klic: y = fcheb(x)
%

function y = fcheb(x)
y = cos(x)+exp(x);
