% Trapezna metoda s kontrolo koraka - uporaba algoritma 5.5.1

a = 3;
b = 6;
N = 20;
epsilon = 1e-1;
EE = [];
TT = [];
FF = [];T = 0;
EPS = [];

for I = 1:8
        alg551
        tic
        alg551
        F = toc
        FF = [FF F];
        EPS = [EPS epsilon];
        epsilon = epsilon/10;
        E = T - 14/3;
        TT = [TT T];
        EE = [EE E];
end
figure(1)
loglog(EPS,abs(EE)), grid
        title('Trapezna metoda s kontrolo koraka')
        xlabel('log(epsilon)')
        ylabel('log(napaka)')
figure(2)
loglog(EPS,FF), grid
        title('Trapezna metoda s kontrolo koraka')
        xlabel('log(epsilon)')
        ylabel('log(cas)')
