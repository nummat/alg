% Adams-Bashforth-Moultonova 
% metoda prediktor-korektor 4. reda - algoritem 6.3.2
% Racunanje resitve diferencialne enacbe
% Podatki:
%               f632            desna stran diferencialne enacbe
%               x0                      zacetna tocka 
%               y0                      zacetna vrednost resitve
%               N                       stevilo korakov
%               h                       dolzina koraka
% Rezultat:
%               Y                       priblizek za resitev diferencialne enacbe

x = x0-3*h; 
for i=1:3
        y = 1+exp(-x); 
        d(i) = f631(x,y);
        x = x+h;         
end
y = y0; Y = [y];
x = x0; X = [x];
d(4) = f631(x,y);

for i = 4:N+3
   x = x+h;     X = [X x];
   yp= y + h*(55*d(4) - 59*d(3) + 37*d(2) - 9*d(1))/24;
        dp = f631(x,yp);
        y = y + h*(9*dp + 19*d(4) - 5*d(3) + d(2))/24;
   Y = [Y y];
   d(5) = f631(x,y);
   for j = 1:4
        d(j) = d(j+1);
   end
end
