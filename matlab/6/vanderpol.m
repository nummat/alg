% Metoda DOPRI5 - uporaba algoritma 6.5.1

f651 = inline('[y(2);(1-y(1)^2)*y(2)-0.1*y(1)]','x','y')
y0 = [-5/2 -3]';
x0 = 0;
b = 250;
epsilon = 1e-4;
h = b-x0;
sigma = 0.8;

alg651

[n,m] = size(X);
figure(3)
    bar(X(2:m), H(2:m))
    title('van der Pol, metoda DOPRI5 reda 5(4)')
    xlabel('x')
    ylabel('Zaporedje korakov')
figure(2)
    plot(Y(1,:),Y(2,:),'g')
    title('van der Pol, metoda DOPRI5 reda 5(4)')
    xlabel('y_1')
    ylabel('y_2')
figure(1)
    plot(X,Y(1,:),'b',X,Y(2,:),'g')
    title('van der Pol, metoda DOPRI5 reda 5(4)')
    ylabel('y_1 (modra) in y_2 (zelena)')
    xlabel('x')
