% Metoda koncnih diferenc - Algoritem 6.9.2
% Racunanje resitve linearnega robnega problema
%               y'' = f692(x)*y' + g692(x)*y + h692(x); y(a) = A, y(b) = B
% Podatki:
%               f692       desna stran diferencialne enacbe, koeficient pri y'
%               g692       desna stran diferencialne enacbe, koeficient pri y
%               h692       desna stran diferencialne enacbe, prosti koeficient
%               a                       zacetna tocka 
%               A                       zacetna vrednost resitve y
%               b                       koncna tocka 
%               B                       koncna vrednost resitve y
%               N                       stevilo korakov
% Rezultat:
%               y                       priblizek za resitev diferencialne enacbe

h = (b-a)/N
x = a+[1:N-1]*h;

M = diag(-2*ones(1,N-1)/h^2 - g692(x))+...
diag(ones(1,N-2)/h^2 + f692(x(2:N-1))/(2*h),-1)+...
diag(ones(1,N-2)/h^2 - f692(x(1:N-2))/(2*h),1);

d = h692(x);
d(1) = d(1)-A*(1/h^2+f692(a)/(2*h));
d(N-1) = d(N-1)-B*(1/h^2-f692(b)/(2*h));
y = M\d';

