% Problem stabilnosti za Eulerjevo metodo
% diferencialna enacba  y' = -8y, y(0) = 1
x0=0;
y0=1;

hold off
f621 = inline('-8*y+0*x');
f641 = inline('-8*y+0*x');
N = 100; h = 1/N; alg621
plot(X,Y), hold on
ylabel('h=1/100'),pause

N = 50; h = 1/N; alg621
plot(X,Y,'m')
ylabel('h=1/50'),pause

N = 20; h = 1/N; alg621
plot(X,Y,'r')
ylabel('h=1/20'), pause

hold off
N = 10; h = 1/N; alg621
plot(X,Y), hold on
ylabel('h=1/10'), pause

N = 8; h = 1/N; alg621
plot(X,Y,'k')
ylabel('h=1/8'), pause

N = 6; h = 1/N; alg621
plot(X,Y,'r'), hold off
ylabel('h=1/6'), pause

N = 5; h = 1/N; alg621
plot(X,Y), hold on
ylabel('h=1/5'), pause

N = 4; h = 1/N; alg621
plot(X,Y,'k')
ylabel('h=1/4'), pause

N = 3; h = 1/N; alg621
plot(X,Y,'m')
ylabel('h=1/4'), pause

N = 2; h = 1/N; alg621
plot(X,Y,'r')
ylabel('h=1/2'), pause



