% Psevdospektralna metoda Chebyseva - Algoritem 6.9.3
% Racunanje resitve linearnega robnega problema
%               y'' = f692(x)*y' + g692(x)*y + h692(x); y(a) = A, y(b) = B
% Podatki:
%               f692       desna stran diferencialne enacbe, koeficient pri y'
%               g692       desna stran diferencialne enacbe, koeficient pri y
%               h692       desna stran diferencialne enacbe, prosti koeficient
%               a                       zacetna tocka 
%               A                       zacetna vrednost resitve y
%               b                       koncna tocka 
%               B                       koncna vrednost resitve y
%               N                       Do polinoma stopnje N = st. notranjih kolokacijskih tock+1
% Rezultat:
%               y                       priblizek za resitev diferencialne enacbe

%               dT in ddT  odvod in drugi odvod polinoma Chebyseva

x = linspace(0,pi/2,N+1)
x = cos(x(2:N))
AA = zeros(N+1,N+1);
for i=0:N
	AA(1,i+1) = cheb(a,i);
	for j=2:N
		AA(j,i+1) = ddT(x(j-1),i)-f692(x(j-1))*dT(x(j-1),i)-g692(x(j-1))*cheb(x(j-1),i);
	end
	AA(N+1,i+1) = cheb(b,i);
end
h = [A; h692(x');B];
			 
ak = AA\h;			 
