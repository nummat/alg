% Sistem linearnih diferencialnih enacb
% Adams-Bashforth-Moulton   in Runge-Kutta 4. reda 
clear

N = 10;
h = 1/10;
x0 = 0; 
y0 = [1 1]';

alg661 %Adams-Bashforth-Moulton

E = Y - [exp(-X); ones(size(X))];
figure(1)
plot(X,Y,'*r',X,[exp(-X); ones(size(X))],'g')
    title('Adams-Bashforth-Moulton 4.reda')
    xlabel('x')
    ylabel('Tocna resitev (zelena) in numericna resitev (rdeca)')
figure(2)
plot(X,E)
    title('Adams-Bashforth-Moulton 4.reda')
    xlabel('x')
    ylabel('Globalna napaka')

alg641s %Runge-Kutta    

E = Y - [exp(-X); ones(size(X))]; xx = [x0:0.0001:x0+N*h];
figure(3)
plot(X,Y,'*r',xx,[exp(-xx); ones(size(xx))],'g')
    title('Runge-Kutta 4.reda')
    xlabel('x')
    ylabel('Tocna resitev (zelena) in numericna resitev (rdeca)')
figure(4)
plot(X,E)
    title('Runge-Kutta 4.reda')
    xlabel('x')
    ylabel('Globalna napaka')
