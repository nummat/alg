% Prediktor korektor
% Adams-Bashforth-Moulton  4. reda - uporaba algoritma 6.3.2

N = 10;
h = 1/10;
x0 = 0; 
y0 = 2;

alg632

E = Y - (exp(-X) + 1);
xx = x0:h/10:N*h;
figure(2)
plot(X,E)
    title('Adams-Bashforth-Moulton 4.reda')
    xlabel('x')
    ylabel('Globalna napaka')
figure(1)
plot(X,Y,'*r',xx,exp(-xx)+1,'g')
    title('Adams-Bashforth-Moulton 4.reda')
    xlabel('x')
    ylabel('Tocna resitev (zelena) in numericna resitev (rdeca)')
