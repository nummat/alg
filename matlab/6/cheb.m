% Izračun vrednosti polinoma Čebiševa stopnje k v točki x
%
% Klic: y = cheb(x,k)
%

function y = cheb(x,k);

if (k==0), y = ones(size(x)); elseif
   (k==1), y = x; else
   ys = ones(size(x));
   yn = x;
   for j=2:k
      y = 2*x.*yn-ys;
      ys=yn; yn=y;
   end
endif
