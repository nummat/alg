% Izračun vrednosti odvoda polinoma Čebiševa stopnje n v točki x
%
% Klic: y = dT(x,n)
%

function y = dT(x,n);

t = acos(x);
y = n.*sin(n*t)/sin(t);
