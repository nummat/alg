% Izračun vrednosti drugega odvoda polinoma Čebiševa stopnje n v točki x
%
% Klic: y = ddT(x,n)
%

function y = ddT(x,n);

t = acos(x); 
y = -n.^2 .*cos(n*t)./(sin(t))^2 + n.*cos(t).*sin(n*t)./(sin(t))^3;
