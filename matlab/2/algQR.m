% Householderjeva QR faktorizacija - Datta algoritem 5.4.3
% 
% Podatki:
%               A            Kvadratna matrika n x n
%
% Rezultat:
%               R            v zgornjem trikotniku matrike A
%							 zgornje trikotna matrika n x n
%               U            vektorji za generacijo Householderjevih matrik H_k, spravljeni
%								v spodnjem trikotniku matrike U
s = zeros(N-1,1);

for i = 1:N-1
  s(i) = sign(A(i,i))*norm(A(i:N,i));
  u = [s(i)+A(i,i); A(i+1:N,i)]; 
  A(i,i) = -s(i);
  s(i) = u(1);
  A(i:N,i+1:N) = A(i:N,i+1:N)-2*u*(u'*A(i:N,i+1:N)/(u'*u));
end
for i = 1:N-1
u = [zeros(i-1,1);s(i);A(i+1:N,i)];
b = b-2*u*(u'*b)/([s(i);A(i+1:N,i)]'*[s(i);A(i+1:N,i)]);
end
U=A;
alg222
