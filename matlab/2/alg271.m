% Iterativno izboljsanje resitve - algoritem 2.7.1
%
% Podatki:
%	A	kvadratna matrika matrika
%	n 	red matrike A
%	b	desna stran sistema Ax=b
%
% Rezultat:
%	x0	zacetni priblizek
%   r0  zacetni ostanek
%   x1  popravljeni priblizek
%   r1  ostanek po popravi

[L,U,P] = lu(A);
y = L\(P*b);
x0 = U\y;

r0 = A*x0-b;
f0 = L\(P*r0);
e0 = U\f0;
x1 = x0-e0;

r1 = A*x1-b;

nr0 =  norm(r0)
nr1 =  norm(r1)